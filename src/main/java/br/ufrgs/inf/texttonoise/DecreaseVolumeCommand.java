package br.ufrgs.inf.texttonoise;

/**
 * Command to decrease the volume of a given conductor.
 */
public class DecreaseVolumeCommand implements Command {
	public Conductor conductor;

	/**
	 * Command to decrease the volume of a conductor
	 * 
	 * @param aConductor
	 *            A Conductor object
	 */
	public DecreaseVolumeCommand(Conductor aConductor) {
		conductor = aConductor;
	}

	@Override
	public void execute() {
		conductor.decreaseVolume();
	}
}