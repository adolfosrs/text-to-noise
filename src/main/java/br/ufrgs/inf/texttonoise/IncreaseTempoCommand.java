package br.ufrgs.inf.texttonoise;

/**
 * Command to increase the tempo of a given conductor.
 */
public class IncreaseTempoCommand implements Command {
	public Conductor conductor;

	/**
	 * Command to increase the tempo of a conductor
	 * 
	 * @param aConductor
	 *            A Conductor object
	 */
	public IncreaseTempoCommand(Conductor aConductor) {
		conductor = aConductor;
	}

	@Override
	public void execute() {
		conductor.increaseTempo();
	}
}