package br.ufrgs.inf.texttonoise;

import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Holds the configuration of the system.
 */
public class Configuration {
	private String notationDictionaryFilepath;
	private NotationDictionary notationDictionary;

	/**
	 * Construct a new Configuration object defining the default options
	 * 
	 * @throws FileNotFoundException
	 */
	public Configuration() throws FileNotFoundException {
		notationDictionaryFilepath = System.getProperty("user.dir") + System.getProperty("file.separator")
				+ "sample-files" + System.getProperty("file.separator") + "sample-dictionary.json";

		FileReader dictionaryFileReader = new FileReader(notationDictionaryFilepath);
		notationDictionary = new NotationDictionary(dictionaryFileReader);
	}

	/**
	 * Set the current system notation dictionary file path
	 * 
	 * @param dictionary
	 *            The path to the dictionary file
	 */
	public void setNotationDictionaryFilepath(String dictionary) {
		notationDictionaryFilepath = dictionary;
	}

	/**
	 * Get the notation dictionary file path
	 * 
	 * @return The dictionary file path
	 */
	public String getNotationDictionaryFilepath() {
		return notationDictionaryFilepath;
	}

	/**
	 * Get the current system notation dictionary
	 * 
	 * @return the notation dictionary
	 */
	public NotationDictionary getNotationDictionary() {
		return notationDictionary;
	}

	/**
	 * Set the current system notation dictionary
	 * 
	 * @param notationDictionary
	 *            The notation dictionary
	 */
	public void setNotationDictionary(NotationDictionary notationDictionary) {
		this.notationDictionary = notationDictionary;
	}
}
