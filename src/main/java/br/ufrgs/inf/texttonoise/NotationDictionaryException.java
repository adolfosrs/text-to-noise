package br.ufrgs.inf.texttonoise;

/**
 * Thrown when there are errors in the Notation Dictionary.
 */
public class NotationDictionaryException extends RuntimeException {
	private static final long serialVersionUID = -4277049420890131756L;

	public NotationDictionaryException() {
		super();
	}

	public NotationDictionaryException(String msg) {
		super(msg);
	}
}
