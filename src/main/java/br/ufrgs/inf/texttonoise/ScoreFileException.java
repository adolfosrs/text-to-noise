package br.ufrgs.inf.texttonoise;

/**
 * Thrown when somebody tries to call an inexistent conductor command.
 */
public class ScoreFileException extends RuntimeException {
	private static final long serialVersionUID = 281673736213861056L;

	public ScoreFileException() {
		super();
	}

	public ScoreFileException(String msg) {
		super(msg);
	}

}
