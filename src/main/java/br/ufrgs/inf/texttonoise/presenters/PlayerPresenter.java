package br.ufrgs.inf.texttonoise.presenters;

import java.io.IOException;

import javax.swing.JOptionPane;

import br.ufrgs.inf.texttonoise.Conductor;
import br.ufrgs.inf.texttonoise.Configuration;
import br.ufrgs.inf.texttonoise.Score;
import br.ufrgs.inf.texttonoise.ScoreFileException;
import br.ufrgs.inf.texttonoise.ScoreFileReader;
import br.ufrgs.inf.texttonoise.ui.PlayerView;

/**
 * The presenter for the Player module
 */
public class PlayerPresenter {
	public final static int initialTempo = (Conductor.maxTempo + Conductor.minTempo) / 2;
	public final static int minTempo = Conductor.minTempo;
	public final static int maxTempo = Conductor.maxTempo;
	public final static int defaultBeatsPerMeasure = Score.defaultBeatsPerMeasure;
	Conductor conductor;
	PlayerView playerView;
	private Configuration config;
	private Score score;

	public Conductor getModel() {
		return conductor;
	}

	public void setModel(Conductor conductor) {
		this.conductor = conductor;
	}

	public PlayerView getView() {
		return playerView;
	}

	public void setView(PlayerView playerView) {
		this.playerView = playerView;
	}

	/**
	 * Controls changes in the tempo in the interface
	 */
	public void changeTempo() {
		playerView.updateModelFromView();
	}

	/**
	 * Initialize the data in the view and set the presenter object in the view
	 */
	public void initialize() {
		playerView.setPresenter(this);
		playerView.updateViewFromModel();
	}

	/**
	 * Handles the selection of the score file
	 */
	public void selectScoreFile() {
		try {
			String filepath = playerView.getScoreFile().getCanonicalPath();
			ScoreFileReader scoreFileReader = new ScoreFileReader(filepath, playerView.getBeatsPerMeasure());
			score = scoreFileReader.getScore();

			// if everything went nice, enable to play
			playerView.enablePlayButton();
			playerView.setTextCurrentScoreFile(filepath);

		} catch (IOException e) {
			String msg = "An error occurred while reading the file.";
			JOptionPane.showMessageDialog(null, msg);
		} catch (ScoreFileException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}

	/**
	 * Set the Configuration object used in the execution
	 * 
	 * @param config
	 *            The Configuration object
	 */
	public void setConfiguration(Configuration config) {
		this.config = config;
	}

	/**
	 * Starts the execution of the score
	 */
	public void playScore() {
		if (!conductor.isAlive()) {
			conductor = new Conductor(config.getNotationDictionary(), score, playerView.getTempo());
			playerView.updateModelFromView();
			conductor.start();
		}
	}

	/**
	 * Stop the execution of the score
	 */
	public void stopExecution() {
		if (conductor.isAlive()) {
			conductor.stopExecution();
		}
	}

	/**
	 * Handles changes in the volume in the interface
	 */
	public void changeVolume() {
		playerView.updateModelFromView();
	}
}
