package br.ufrgs.inf.texttonoise.presenters;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JOptionPane;

import br.ufrgs.inf.texttonoise.Configuration;
import br.ufrgs.inf.texttonoise.NotationDictionary;
import br.ufrgs.inf.texttonoise.NotationDictionaryException;
import br.ufrgs.inf.texttonoise.ui.ConfigurationsView;

/**
 * The presenter for the Configurations module
 */
public class ConfigurationsPresenter {
	ConfigurationsView configView;
	Configuration configuration;

	public void setView(ConfigurationsView configurationView) {
		this.configView = configurationView;
	}

	public ConfigurationsView getView() {
		return (configView);
	}

	public void setModel(Configuration config) {
		configuration = config;
	}

	public Configuration getModel() {
		return configuration;
	}

	/**
	 * Change the notation dictionary used in the system
	 */
	public void changeNotationDictionary() {
		File dictionaryFile = getView().getDictionaryFile();
		try {
			// First, try to read the file
			FileReader dictionaryFileReader = new FileReader(dictionaryFile);

			// Secondly, try to parse the file
			NotationDictionary dictionary = new NotationDictionary(dictionaryFileReader);

			// Only if everything went nice update the model
			String dictionaryFilePath = dictionaryFile.getCanonicalPath();
			configuration.setNotationDictionaryFilepath(dictionaryFilePath);
			configuration.setNotationDictionary(dictionary);
			configView.updateViewFromModel();
		} catch (IOException e) {
			String msg = "An error occurred while reading the file.";
			JOptionPane.showMessageDialog(null, msg);
		} catch (NotationDictionaryException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}

	/**
	 * Set the presenter object in the view and set the information from the
	 * model in the view.
	 */
	public void initialize() {
		configView.setPresenter(this);
		configView.updateViewFromModel();
	}
}
