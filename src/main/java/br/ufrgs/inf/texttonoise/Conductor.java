package br.ufrgs.inf.texttonoise;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Represents a Conductor
 */
public class Conductor extends Thread {
	public static final int maxTempo = 2000;
	public static final int minTempo = 1;
	public static final int initialVolume = 80;
	private NotationDictionary dictionary;
	private Score score;
	private List<Character> lastMeasure = new ArrayList<Character>();;
	private int tempo = (maxTempo + minTempo) / 2;
	private String logMessage = "";
	private int currentVolume = initialVolume;
	private ConductorCommandsMap commandMap = new ConductorCommandsMap(this);
	private boolean enableExecution = true;

	/**
	 * Creates a conductor object with a score and an initial tempo in bpm.
	 * 
	 * @param dictionary
	 *            The dictionary used by the conductor to interpret the score
	 * @param score
	 *            The score to lead the conductor
	 * @param tempo
	 *            The tempo in BPM
	 */

	public Conductor(NotationDictionary dictionary, Score score, int tempo) {
		if (tempo > 0 && isValidTempo(tempo)) {
			this.dictionary = dictionary;
			this.score = score;
			this.tempo = tempo;
		} else {
			throw new IllegalArgumentException("The initial tempo must be greater than 0");
		}
	}

	public Conductor(NotationDictionary dictionary) {
		this.dictionary = dictionary;
	}

	public void setScore(Score score) {
		this.score = score;
	}

	private boolean isValidTempo(int tempo) {
		return tempo >= minTempo && tempo <= maxTempo;
	}

	/**
	 * Increase the tempo by one unit until the maxTempo limit.
	 */
	public void increaseTempo() {
		if ((tempo + 1) < maxTempo) {
			tempo += 1;
		}
	}

	/**
	 * Decrease the tempo by one unit until the minTempo limit.
	 */
	public void decreaseTempo() {
		if ((tempo - 1) > minTempo) {
			tempo -= 1;
		}
	}

	public void setTempo(int newTempo) {
		if (newTempo > maxTempo) {
			tempo = maxTempo;
		} else if (newTempo < minTempo) {
			tempo = minTempo;
		} else {
			tempo = newTempo;
		}
	}

	/**
	 * Increase the execution volume
	 */
	public void increaseVolume() {
		if (this.currentVolume < 127) {
			this.currentVolume++;
		}
	}

	/**
	 * Decrease the execution volume
	 */
	public void decreaseVolume() {
		if (this.currentVolume > 0) {
			this.currentVolume--;
		}
	}

	/**
	 * Sets the current volume
	 */
	public void setVolume(int volume) {
		if (volume >= 0 && volume <= 127) {
			currentVolume = volume;
		}
	}

	/**
	 * Return the current volume
	 * 
	 * @return The current volume
	 */
	public int getCurrentVolume() {
		return this.currentVolume;
	}

	/**
	 * Return the current tempo.
	 * 
	 * @return The current tempo.
	 */
	public int getTempo() {
		return tempo;
	}

	private void addSymbolToLastMeasure(char symbol) {
		if (lastMeasure.size() < score.getBeatsPerMeasure()) {
			lastMeasure.add(symbol);
		} else {
			lastMeasure.clear();
			lastMeasure.add(symbol);
		}
	}

	/**
	 * Play the last measure notes again.
	 */
	public void playLastMeasure() {
		for (Character symbol : lastMeasure) {
			if (dictionary.isNote(symbol)) {
				Musician musician = new Musician(dictionary.getInstrument(symbol));
				musician.play(dictionary.getNote(symbol), timePerBeat(), this.currentVolume);
			} else {
				insertPause();
			}
		}

		logMessage += "played-last-measure ";
	}

	/**
	 * Run the thread
	 */
	@Override
	public void run() {
		startConcert();
	}

	/**
	 * Starts to play the score.
	 */
	public void startConcert() {
		Iterator<Character> scoreSymbols = score.getSymbols().iterator();

		logMessage += "started-concert ";

		while (scoreSymbols.hasNext() && enableExecution) {
			Character symbol = scoreSymbols.next();

			if (dictionary.isNote(symbol)) {
				addSymbolToLastMeasure(symbol);
				Musician musician = new Musician(dictionary.getInstrument(symbol));
				musician.play(dictionary.getNote(symbol), timePerBeat(), this.currentVolume);
			} else if (dictionary.isCommand(symbol)) {
				String commandName = dictionary.getConductorCommand(symbol);
				commandMap.getCommand(commandName).execute();
			} else {
				// If could not find the symbol in the dictionary, insert a
				// pause for one beat and add a pause to the last measure
				addSymbolToLastMeasure(symbol);
				insertPause();
			}
		}

		enableExecution = true;

		logMessage += "finished-concert ";
	}

	/**
	 * Stop the thread
	 */
	public void stopExecution() {
		enableExecution = false;
	}

	private void insertPause() {
		try {
			Thread.sleep(timePerBeat());
		} catch (InterruptedException e) {
		}
	}

	public String getLogMessage() {
		return logMessage;
	}

	/**
	 * Returns the time each beat takes
	 * 
	 * @return The time for one beat in milliseconds
	 */
	private int timePerBeat() {
		return (int) ((60.0 / tempo) * 1000);
	}

	/**
	 * Changes the Notation Dictionary
	 * 
	 * @param notationDictionary
	 *            The new dictionary
	 */
	public void setNotationDictionary(NotationDictionary notationDictionary) {
		this.dictionary = notationDictionary;
	}

	/**
	 * return the current score
	 * 
	 * @return the score
	 */
	public Score getScore() {
		return score;
	}
}
