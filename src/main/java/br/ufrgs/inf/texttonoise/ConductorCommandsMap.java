package br.ufrgs.inf.texttonoise;

import java.util.HashMap;
import java.util.Map;

/**
 * Maps the possible concrete commands.
 */
public class ConductorCommandsMap {
	Map<String, Command> commandMap;

	/**
	 * Creates new ConductorCommandsMap
	 * 
	 * @param receiver
	 *            The conductor that will be affected by the commands
	 */
	public ConductorCommandsMap(Conductor receiver) {
		commandMap = new HashMap<String, Command>();
		commandMap.put("increaseTempo", new IncreaseTempoCommand(receiver));
		commandMap.put("decreaseTempo", new DecreaseTempoCommand(receiver));
		commandMap.put("increaseVolume", new IncreaseVolumeCommand(receiver));
		commandMap.put("decreaseVolume", new DecreaseVolumeCommand(receiver));
		commandMap.put("playLastMeasure", new PlayLastMeasureCommand(receiver));
	}

	/**
	 * Get the Command object of the given command name
	 * 
	 * @param commandName
	 *            The name of the command
	 * @return A Command object
	 */
	public Command getCommand(String commandName) {
		if (commandMap.containsKey(commandName)) {
			return commandMap.get(commandName);
		} else {
			String msg = String.format("Unknown command %s.", commandName);
			throw new InvalidCommandException(msg);
		}
	}
}
