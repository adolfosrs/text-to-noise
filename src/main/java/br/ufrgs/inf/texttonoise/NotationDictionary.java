package br.ufrgs.inf.texttonoise;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * This class handles the notation dictionary.
 */
public class NotationDictionary {
	private FileReader dictionaryFile;
	private Map<Character, Integer> notes = new HashMap<Character, Integer>();
	private Map<Character, Integer> instruments = new HashMap<Character, Integer>();
	private Map<Character, String> commands = new HashMap<Character, String>();

	/**
	 * Constructs a new NotesDictionary using the parameter file as dictionary
	 * file
	 * 
	 * @param dictionaryFile
	 *            A FileReader for the dictionary file
	 */
	public NotationDictionary(FileReader dictionaryFile) {
		this.dictionaryFile = dictionaryFile;
		this.load();
	}

	/**
	 * Load the Notation Dictionary from a JSON file
	 * 
	 * @return void
	 * @throws NotationDictionaryException
	 *             when could not parse the dictionary file.
	 */
	private void load() {
		try {
			JSONParser parser = new JSONParser();
			JSONObject jsonElement = (JSONObject) parser.parse(dictionaryFile);
			JSONObject notesFromJson = (JSONObject) jsonElement.get("note-instruments");
			JSONObject commandsFromJson = (JSONObject) jsonElement.get("commands");

			loadNotesAndInstruments(notesFromJson);
			loadCommands(commandsFromJson);
		} catch (NotationDictionaryException e) {
			throw e;
		} catch (Exception e) {
			throw new NotationDictionaryException("Could not parse the dictionary file.");
		}
	}

	private void loadCommands(JSONObject commandsFromJson) {
		// commands HashMap
		Iterator<?> commandsInform = commandsFromJson.values().iterator();
		Iterator<?> commandsName = commandsFromJson.keySet().iterator();
		while (commandsName.hasNext()) {
			// takes name of the command
			String nameCommand = commandsName.next().toString();
			char[] nameChar = nameCommand.toCharArray();
			// takes the functionality of the command
			String commandName = commandsInform.next().toString();
			// mount command HashMap
			this.commands.put(nameChar[0], commandName);
		}
	}

	private void loadNotesAndInstruments(JSONObject notesFromJson) {
		// noteInstruments HashMap
		Iterator<?> noteInform = notesFromJson.values().iterator();
		Iterator<?> note = notesFromJson.keySet().iterator();
		while (note.hasNext()) {
			// takes name of the note
			String nameNote = note.next().toString();
			char[] nameChar = nameNote.toCharArray();
			// takes the note number and the number of the instrument
			JSONObject noteJsonInform = (JSONObject) noteInform.next();
			int noteNumber = Integer.parseInt(noteJsonInform.get("note-number").toString());
			int instrumentNumber = Integer.parseInt(noteJsonInform.get("instrument-number").toString());

			if (noteNumber >= 0 && noteNumber <= 127) {
				notes.put(nameChar[0], noteNumber);
			} else {
				String msg = String.format("Invalid note on dictionary.\nCode: %s. Note: %d.", nameChar[0], noteNumber);
				throw new NotationDictionaryException(msg);
			}

			if (instrumentNumber >= 0 && instrumentNumber <= 127) {
				instruments.put(nameChar[0], instrumentNumber);
			} else {
				String msg = String.format("Invalid instrument on dictionary. Code: %s Note: %d", nameChar[0],
						instrumentNumber);
				throw new NotationDictionaryException(msg);
			}
		}
	}

	/**
	 * Get the note number from a given note code
	 * 
	 * @param noteCode
	 *            The text code representing the note
	 * @return The note number
	 */
	public int getNote(char noteCode) {
		return notes.get(noteCode);
	}

	/**
	 * Get the note number from a given instrument code.
	 * 
	 * @param instrumentCode
	 *            The text code representing the instrument
	 * @return The instrument number
	 */
	public int getInstrument(char instrumentCode) {
		return instruments.get(instrumentCode);
	}

	/**
	 * Checks whether a given note code is an existing note or not.
	 * 
	 * @param noteCode
	 *            The text code representing the note
	 * @return True if it is, not otherwise
	 */
	public boolean isNote(char noteCode) {
		return notes.containsKey(noteCode);
	}

	/**
	 * Checks whether a given note code is an existing instrument or not.
	 * 
	 * @param instrumentCode
	 *            The text code representing the note
	 * @return True if it is, not otherwise
	 */
	public boolean isInstrument(char instrumentCode) {
		return instruments.containsKey(instrumentCode);
	}

	/**
	 * Returns the Conductor Command object from the given code.
	 * 
	 * @param commandCode
	 *            The command code
	 * @return A conductor command object
	 */
	public String getConductorCommand(char commandCode) {
		return this.commands.get(commandCode);
	}

	/**
	 * Verifies if the parameter is a command, if so, returns true, else return
	 * false
	 * 
	 * @param command
	 *            the value that will be verified
	 * @return boolean
	 */
	public boolean isCommand(char command) {
		return this.commands.containsKey(command);
	}
}
