package br.ufrgs.inf.texttonoise;

/**
 * A Command interface (GOF Pattern).
 */
public interface Command {

	/**
	 * Executes the command.
	 */
	public abstract void execute();
}