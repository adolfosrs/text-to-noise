package br.ufrgs.inf.texttonoise;

/**
 * Command to increase the volume of a given conductor.
 */
public class IncreaseVolumeCommand implements Command {
	public Conductor conductor;

	/**
	 * Command to increase the volume of a conductor
	 * 
	 * @param aConductor
	 *            A Conductor object
	 */
	public IncreaseVolumeCommand(Conductor aConductor) {
		conductor = aConductor;
	}

	@Override
	public void execute() {
		conductor.increaseVolume();
	}
}