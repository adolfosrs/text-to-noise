package br.ufrgs.inf.texttonoise.ui;

import java.io.File;

import br.ufrgs.inf.texttonoise.presenters.PlayerPresenter;

/**
 * The Player view interface.
 */
public interface PlayerView {

	/**
	 * Get the presenter of this view
	 * 
	 * @return Player Presenter
	 */
	PlayerPresenter getPresenter();

	/**
	 * Set the presenter for this view
	 * 
	 * @param aPlayerPresenter
	 *            A Player Presenter
	 */
	void setPresenter(PlayerPresenter aPlayerPresenter);

	/**
	 * Updates the view with the model data.
	 */
	void updateViewFromModel();

	/**
	 * Updates the model with the data in the view
	 */
	void updateModelFromView();

	/**
	 * Set the current score file path in the view
	 * 
	 * @param textCurrentScoreFile
	 *            The filepath to the file
	 */
	void setTextCurrentScoreFile(String textCurrentScoreFile);

	/**
	 * Selects the file to be played
	 * 
	 * @return Trueif the user selected a file or false otherwise
	 */
	public boolean selectFile();

	/**
	 * Get the score file selected by the user
	 * 
	 * @return The score file
	 */
	public File getScoreFile();

	/**
	 * Sets the initial beats per measure
	 * 
	 * @param beatsPerMeasure
	 *            The number of beats per measure
	 */
	public void setBeatsPerMeasure(int beatsPerMeasure);

	/**
	 * Get the current beats per measure
	 * 
	 * @return The current number of beats per measure
	 */
	public int getBeatsPerMeasure();

	/**
	 * Set the initial tempo
	 * 
	 * @param tempo
	 *            The tempo in BPM
	 */
	public void setTempo(int tempo);

	/**
	 * Get the current tempo
	 * 
	 * @return The tempo in BPM
	 */
	public int getTempo();

	/**
	 * Enable the play button
	 */
	public void enablePlayButton();

	/**
	 * Disable the play button
	 */
	public void disablePlayButton();

	/**
	 * Get the current volume
	 * 
	 * @return The volume
	 */
	int getVolume();

	/**
	 * Set the volume
	 * 
	 * @param volume
	 *            The volume
	 */
	void setVolume(int volume);
}
