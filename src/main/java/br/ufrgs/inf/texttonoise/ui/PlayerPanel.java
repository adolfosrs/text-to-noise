package br.ufrgs.inf.texttonoise.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import br.ufrgs.inf.texttonoise.Conductor;
import br.ufrgs.inf.texttonoise.presenters.PlayerPresenter;

/**
 * Concrete Player View. Implements the view using a Swing JPanel.
 */
public class PlayerPanel extends JPanel implements PlayerView {

	private static final long serialVersionUID = -4669823629761705524L;
	private PlayerPresenter playerPresenter;
	private JButton btnOpenFile;
	private JSpinner tempoSpinner;
	private JSpinner beatsPerMeasureSpinner;
	private JButton btnPlay;
	private File scoreFile;
	private JTextField textCurrentScoreFile;
	private JButton btnStop;
	private JLabel lblVolume;
	private JSlider volumeSlider;

	/**
	 * Create the panel.
	 */
	public PlayerPanel() {
		setBackground(SystemColor.control);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		btnOpenFile = new JButton("Open File");
		btnOpenFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				selectScoreFile(e);
			}
		});
		GridBagConstraints gbc_btnOpenFile = new GridBagConstraints();
		gbc_btnOpenFile.anchor = GridBagConstraints.EAST;
		gbc_btnOpenFile.insets = new Insets(0, 0, 5, 5);
		gbc_btnOpenFile.gridx = 1;
		gbc_btnOpenFile.gridy = 1;
		add(btnOpenFile, gbc_btnOpenFile);

		textCurrentScoreFile = new JTextField();
		textCurrentScoreFile.setText("Open a score file...");
		textCurrentScoreFile.setEnabled(false);
		textCurrentScoreFile.setEditable(false);
		GridBagConstraints gbc_textCurrentScoreFile = new GridBagConstraints();
		gbc_textCurrentScoreFile.fill = GridBagConstraints.HORIZONTAL;
		gbc_textCurrentScoreFile.insets = new Insets(0, 0, 5, 0);
		gbc_textCurrentScoreFile.gridx = 2;
		gbc_textCurrentScoreFile.gridy = 1;
		add(textCurrentScoreFile, gbc_textCurrentScoreFile);
		textCurrentScoreFile.setColumns(10);

		JLabel lblTempo = new JLabel("Tempo:");
		GridBagConstraints gbc_lblTempo = new GridBagConstraints();
		gbc_lblTempo.anchor = GridBagConstraints.EAST;
		gbc_lblTempo.insets = new Insets(0, 0, 5, 5);
		gbc_lblTempo.gridx = 1;
		gbc_lblTempo.gridy = 2;
		add(lblTempo, gbc_lblTempo);

		tempoSpinner = new JSpinner();
		tempoSpinner.setModel(new SpinnerNumberModel(1000, 1, 2000, 50));
		tempoSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent evt) {
				changeTempo(evt);
			}
		});
		GridBagConstraints gbc_tempoSpinner = new GridBagConstraints();
		gbc_tempoSpinner.anchor = GridBagConstraints.WEST;
		gbc_tempoSpinner.insets = new Insets(0, 0, 5, 0);
		gbc_tempoSpinner.gridx = 2;
		gbc_tempoSpinner.gridy = 2;
		add(tempoSpinner, gbc_tempoSpinner);

		JLabel lblBeatspermeasure = new JLabel("BeatsPerMeasure");
		GridBagConstraints gbc_lblBeatspermeasure = new GridBagConstraints();
		gbc_lblBeatspermeasure.anchor = GridBagConstraints.EAST;
		gbc_lblBeatspermeasure.insets = new Insets(0, 0, 5, 5);
		gbc_lblBeatspermeasure.gridx = 1;
		gbc_lblBeatspermeasure.gridy = 3;
		add(lblBeatspermeasure, gbc_lblBeatspermeasure);

		beatsPerMeasureSpinner = new JSpinner();
		beatsPerMeasureSpinner.setModel(new SpinnerNumberModel(PlayerPresenter.defaultBeatsPerMeasure,
				new Integer(1), null, new Integer(1)));
		GridBagConstraints gbc_beatsPerMeasureSpinner = new GridBagConstraints();
		gbc_beatsPerMeasureSpinner.anchor = GridBagConstraints.WEST;
		gbc_beatsPerMeasureSpinner.insets = new Insets(0, 0, 5, 0);
		gbc_beatsPerMeasureSpinner.gridx = 2;
		gbc_beatsPerMeasureSpinner.gridy = 3;
		add(beatsPerMeasureSpinner, gbc_beatsPerMeasureSpinner);

		btnStop = new JButton("Stop");
		btnStop.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				stopButtonPressed(e);
			}
		});

		btnPlay = new JButton("Play");
		btnPlay.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				playButtonPressed(e);
			}
		});

		lblVolume = new JLabel("Volume");
		GridBagConstraints gbc_lblVolume = new GridBagConstraints();
		gbc_lblVolume.anchor = GridBagConstraints.EAST;
		gbc_lblVolume.insets = new Insets(0, 0, 5, 5);
		gbc_lblVolume.gridx = 1;
		gbc_lblVolume.gridy = 4;
		add(lblVolume, gbc_lblVolume);

		volumeSlider = new JSlider(SwingConstants.HORIZONTAL);
		volumeSlider.setMaximum(127);
		volumeSlider.setMinimum(0);
		volumeSlider.setValue(Conductor.initialVolume);
		GridBagConstraints gbc_volumeSlider = new GridBagConstraints();
		gbc_volumeSlider.anchor = GridBagConstraints.WEST;
		gbc_volumeSlider.insets = new Insets(0, 0, 5, 0);
		gbc_volumeSlider.gridx = 2;
		gbc_volumeSlider.gridy = 4;
		add(volumeSlider, gbc_volumeSlider);
		volumeSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				changeVolume(e);
			}
		});
		btnPlay.setEnabled(false);
		GridBagConstraints gbc_btnPlay = new GridBagConstraints();
		gbc_btnPlay.insets = new Insets(0, 0, 0, 5);
		gbc_btnPlay.anchor = GridBagConstraints.EAST;
		gbc_btnPlay.gridx = 1;
		gbc_btnPlay.gridy = 6;
		add(btnPlay, gbc_btnPlay);
		btnStop.setEnabled(false);
		GridBagConstraints gbc_btnStop = new GridBagConstraints();
		gbc_btnStop.anchor = GridBagConstraints.WEST;
		gbc_btnStop.gridx = 2;
		gbc_btnStop.gridy = 6;
		add(btnStop, gbc_btnStop);
	}

	@Override
	public void setTextCurrentScoreFile(String textCurrentScoreFile) {
		this.textCurrentScoreFile.setText(textCurrentScoreFile);
	}

	@Override
	public boolean selectFile() {
		JFileChooser fc = new JFileChooser();
		int returnVal = fc.showOpenDialog(this);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			scoreFile = fc.getSelectedFile();
			return true;
		}

		return false;
	}

	@Override
	public File getScoreFile() {
		return scoreFile;
	}

	@Override
	public void setBeatsPerMeasure(int beatsPerMeasure) {
		beatsPerMeasureSpinner.setValue(beatsPerMeasureSpinner);

	}

	@Override
	public int getBeatsPerMeasure() {
		return (int) beatsPerMeasureSpinner.getValue();
	}

	@Override
	public void setTempo(int tempo) {
		tempoSpinner.setValue(tempo);
	}

	@Override
	public int getTempo() {
		return (int) tempoSpinner.getValue();
	}

	@Override
	public void setVolume(int volume) {
		volumeSlider.setValue(volume);
	}

	@Override
	public int getVolume() {
		return volumeSlider.getValue();
	}

	@Override
	public void enablePlayButton() {
		btnPlay.setEnabled(true);
	}

	@Override
	public void disablePlayButton() {
		btnPlay.setEnabled(false);

	}

	@Override
	public PlayerPresenter getPresenter() {
		return playerPresenter;
	}

	@Override
	public void setPresenter(PlayerPresenter aPlayerPresenter) {
		playerPresenter = aPlayerPresenter;

	}

	@Override
	public void updateViewFromModel() {
		int currentTempo = playerPresenter.getModel().getTempo();
		tempoSpinner.setValue(currentTempo);
	}

	@Override
	public void updateModelFromView() {
		int currentTempo = (int) tempoSpinner.getValue();
		int currentBeatsPerMeasure = (int) beatsPerMeasureSpinner.getValue();
		int currentVolume = volumeSlider.getValue();
		playerPresenter.getModel().setTempo(currentTempo);
		playerPresenter.getModel().setVolume(currentVolume);
		if (playerPresenter.getModel().getScore() != null) {
			playerPresenter.getModel().getScore().setBeatsPerMeasure(currentBeatsPerMeasure);
		}
	}

	private void changeTempo(ChangeEvent e) {
		getPresenter().changeTempo();
	}

	private void changeVolume(ChangeEvent e) {
		getPresenter().changeVolume();
	}

	private void playButtonPressed(ActionEvent e) {
		getPresenter().playScore();
		btnPlay.setEnabled(false);
		btnStop.setEnabled(true);
	}

	private void stopButtonPressed(ActionEvent e) {
		getPresenter().stopExecution();
		btnPlay.setEnabled(true);
		btnStop.setEnabled(false);
	}

	private void selectScoreFile(ActionEvent e) {
		JFileChooser fc = new JFileChooser();
		int returnVal = fc.showOpenDialog(this);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			scoreFile = fc.getSelectedFile();
			getPresenter().selectScoreFile();
		}
	}

}
