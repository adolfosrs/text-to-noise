package br.ufrgs.inf.texttonoise.ui;

import br.ufrgs.inf.texttonoise.*;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JPanel;

import java.awt.TextArea;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

public class ComposerView extends JPanel {

	private static final long serialVersionUID = -4669823629761705524L;
	private final JLabel lblScr = new JLabel("scr/test/resources/");
	private JTextField txtNewcomposition;

	/**
	 * Create the panel.
	 */
	public ComposerView(final NotationDictionary dictionary) {
		setBackground(Color.WHITE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);
		
		JLabel lblNewLabel = new JLabel("");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel.gridx = 14;
		gbc_lblNewLabel.gridy = 1;
		add(lblNewLabel, gbc_lblNewLabel);
		
		final TextArea textArea = new TextArea();
		textArea.setText("Insert here your composition...");
		textArea.setColumns(30);
		textArea.setRows(10);
		GridBagConstraints gbc_textArea = new GridBagConstraints();
		gbc_textArea.gridheight = 5;
		gbc_textArea.gridwidth = 8;
		gbc_textArea.insets = new Insets(0, 0, 5, 5);
		gbc_textArea.gridx = 0;
		gbc_textArea.gridy = 2;
		add(textArea, gbc_textArea);
		
		final JSpinner spinner = new JSpinner();
		final JSpinner spinner_1 = new JSpinner();
		final JSlider slider = new JSlider();
		JButton btnTocar = new JButton("Play");
		btnTocar.setVerticalAlignment(SwingConstants.BOTTOM);
		btnTocar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				char[] text = (char[])textArea.getText().toCharArray();
				Score score = new Score((Integer)spinner_1.getValue());
				int numberChar = text.length;
				for(int i=0;i<numberChar; i++){
					score.addSymbol(text[i]);
				}
				Conductor conductor= new Conductor(dictionary,score,(Integer) spinner.getValue());
				conductor.setVolume((Integer) slider.getValue());
				conductor.startConcert();
			}
		});
		GridBagConstraints gbc_btnTocar = new GridBagConstraints();
		gbc_btnTocar.insets = new Insets(0, 0, 5, 5);
		gbc_btnTocar.gridx = 12;
		gbc_btnTocar.gridy = 2;
		add(btnTocar, gbc_btnTocar);
		
		JLabel lblDuration = new JLabel("Tempo:");
		GridBagConstraints gbc_lblDuration = new GridBagConstraints();
		gbc_lblDuration.insets = new Insets(0, 0, 5, 5);
		gbc_lblDuration.gridx = 12;
		gbc_lblDuration.gridy = 3;
		add(lblDuration, gbc_lblDuration);
		
		
		spinner.setModel(new SpinnerNumberModel(new Integer(1000), null, null, new Integer(1)));
		GridBagConstraints gbc_spinner = new GridBagConstraints();
		gbc_spinner.insets = new Insets(0, 0, 5, 5);
		gbc_spinner.gridx = 12;
		gbc_spinner.gridy = 4;
		add(spinner, gbc_spinner);
		
		JLabel lblBitspermeasure = new JLabel("BitsPerMeasure:");
		GridBagConstraints gbc_lblBitspermeasure = new GridBagConstraints();
		gbc_lblBitspermeasure.insets = new Insets(0, 0, 5, 5);
		gbc_lblBitspermeasure.gridx = 12;
		gbc_lblBitspermeasure.gridy = 5;
		add(lblBitspermeasure, gbc_lblBitspermeasure);
		
		spinner_1.setModel(new SpinnerNumberModel(new Integer(4), null, null, new Integer(1)));
		GridBagConstraints gbc_spinner_1 = new GridBagConstraints();
		gbc_spinner_1.insets = new Insets(0, 0, 5, 5);
		gbc_spinner_1.gridx = 12;
		gbc_spinner_1.gridy = 6;
		add(spinner_1, gbc_spinner_1);
		
		JLabel lblVolume = new JLabel("Volume:");
		GridBagConstraints gbc_lblVolume = new GridBagConstraints();
		gbc_lblVolume.insets = new Insets(0, 0, 5, 5);
		gbc_lblVolume.gridx = 12;
		gbc_lblVolume.gridy = 8;
		add(lblVolume, gbc_lblVolume);
		
		slider.setMaximum(127);
		GridBagConstraints gbc_slider = new GridBagConstraints();
		gbc_slider.ipadx = 99;
		gbc_slider.gridwidth = 3;
		gbc_slider.insets = new Insets(0, 0, 5, 0);
		gbc_slider.gridx = 12;
		gbc_slider.gridy = 9;
		add(slider, gbc_slider);
		
		txtNewcomposition = new JTextField();
		
		
		JButton btnNewButton = new JButton("Save");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String pathName = "src/test/resources/".concat(txtNewcomposition.getText());
				try {
					File file = new File(pathName);
					FileWriter writer = new FileWriter(file);
					writer.write(textArea.getText());	
					writer.close();
				} catch (IOException e1) {

				}
			}
		});
		btnNewButton.setVerticalAlignment(SwingConstants.TOP);
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.insets = new Insets(0, 0, 5, 5);
		gbc_btnNewButton.gridx = 1;
		gbc_btnNewButton.gridy = 10;
		add(btnNewButton, gbc_btnNewButton);
		GridBagConstraints gbc_lblScr = new GridBagConstraints();
		gbc_lblScr.insets = new Insets(0, 0, 5, 5);
		gbc_lblScr.gridx = 2;
		gbc_lblScr.gridy = 10;
		add(lblScr, gbc_lblScr);
		
		txtNewcomposition.setText("NewComposition");
		GridBagConstraints gbc_txtNewcomposition = new GridBagConstraints();
		gbc_txtNewcomposition.insets = new Insets(0, 0, 5, 5);
		gbc_txtNewcomposition.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtNewcomposition.gridx = 3;
		gbc_txtNewcomposition.gridy = 10;
		add(txtNewcomposition, gbc_txtNewcomposition);
		txtNewcomposition.setColumns(10);
	}

}
