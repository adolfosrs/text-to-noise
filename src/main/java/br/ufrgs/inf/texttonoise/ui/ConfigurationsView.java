package br.ufrgs.inf.texttonoise.ui;

import java.io.File;

import br.ufrgs.inf.texttonoise.presenters.ConfigurationsPresenter;

/**
 * The view interface of the configuration
 */
public interface ConfigurationsView {
	/**
	 * Get the Configuration presenter
	 * 
	 * @return a configuration presenter
	 */
	ConfigurationsPresenter getPresenter();

	/**
	 * Set the presenter for this view
	 * 
	 * @param configurationsPresenter
	 */
	void setPresenter(ConfigurationsPresenter configurationsPresenter);

	/**
	 * Update the model getting the data from the view
	 */
	void updateModelFromView();

	/**
	 * Update the view getting the data from the model
	 */
	void updateViewFromModel();

	/**
	 * Get the dictionary file beeing used
	 * 
	 * @return a file
	 */
	public File getDictionaryFile();
}
