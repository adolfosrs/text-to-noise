package br.ufrgs.inf.texttonoise.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.FileReader;

import org.junit.Before;
import org.junit.Test;

import br.ufrgs.inf.texttonoise.NotationDictionary;

public class NotationDictionaryTest {
	NotationDictionary dictionary;

	@Before
	public void loadDictionary() throws FileNotFoundException {
		String testFilePath = "src/test/resources/test-notes-execution-codes.json";
		FileReader notesFile = new FileReader(testFilePath);
		dictionary = new NotationDictionary(notesFile);
	}

	@Test
	public void testGetNote() {
		assertEquals(60, dictionary.getNote('C'));
		assertEquals(62, dictionary.getNote('D'));
		assertEquals(64, dictionary.getNote('E'));
		assertEquals(65, dictionary.getNote('F'));
		assertEquals(67, dictionary.getNote('G'));
		assertEquals(69, dictionary.getNote('A'));
		assertEquals(71, dictionary.getNote('B'));
	}

	@Test
	public void testGetInstrument() {
		assertEquals(1, dictionary.getInstrument('C'));
		assertEquals(2, dictionary.getInstrument('D'));
		assertEquals(3, dictionary.getInstrument('E'));
		assertEquals(4, dictionary.getInstrument('F'));
		assertEquals(5, dictionary.getInstrument('G'));
		assertEquals(6, dictionary.getInstrument('A'));
		assertEquals(7, dictionary.getInstrument('B'));
	}

	@Test
	public void testGetConductorCommand() {
		assertEquals("increaseTempo", dictionary.getConductorCommand('>'));
		assertEquals("decreaseTempo", dictionary.getConductorCommand('<'));
		assertEquals("increaseVolume", dictionary.getConductorCommand('+'));
		assertEquals("decreaseVolume", dictionary.getConductorCommand('-'));
	}

	@Test
	public void testIsNote() {
		assertTrue(dictionary.isInstrument('C'));
		assertFalse(dictionary.isInstrument('5'));
	}

	@Test
	public void testIsInstrument() {
		assertTrue(dictionary.isInstrument('C'));
		assertFalse(dictionary.isInstrument('5'));
		assertFalse(dictionary.isInstrument('+'));
	}

	@Test
	public void testIsCommand() {
		assertTrue(dictionary.isCommand('+'));
		assertFalse(dictionary.isCommand('f'));
		assertFalse(dictionary.isCommand('A'));
	}
}
