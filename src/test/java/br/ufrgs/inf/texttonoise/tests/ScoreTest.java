package br.ufrgs.inf.texttonoise.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;

import br.ufrgs.inf.texttonoise.Score;

public class ScoreTest {

	@Test
	public void testEquals() {
		Score score1 = new Score(4);
		score1.addSymbol('C');
		score1.addSymbol('D');
		score1.addSymbol('E');
		score1.addSymbol('F');

		Score score2 = new Score(4);
		score2.addSymbol('C');
		score2.addSymbol('D');
		score2.addSymbol('E');
		score2.addSymbol('F');

		Score score3 = new Score(50);
		score3.addSymbol('C');
		score3.addSymbol('D');
		score3.addSymbol('E');
		score3.addSymbol('F');

		Score score4 = new Score(4);
		score4.addSymbol('F');
		score4.addSymbol('F');
		score4.addSymbol('F');
		score4.addSymbol('F');

		Score score5 = new Score(50);
		score5.addSymbol('F');
		score5.addSymbol('F');
		score5.addSymbol('F');
		score5.addSymbol('F');

		Score score6 = new Score(50);

		Score score7 = new Score(50);
		score3.addSymbol('C');
		score3.addSymbol('D');
		score3.addSymbol('E');
		score3.addSymbol('F');
		score3.addSymbol('F');

		assertEquals(score1, score2);
		assertFalse(score1.equals(score3));
		assertFalse(score1.equals(score4));
		assertFalse(score1.equals(score5));
		assertFalse(score1.equals(score6));
		assertFalse(score1.equals(score7));
	}

	@Test
	public void testAddNote() throws FileNotFoundException {
		Score score = new Score(4);
		score.addSymbol('C');
		score.addSymbol('D');
		score.addSymbol('E');
		score.addSymbol('F');

		// the expected result
		ArrayList<Character> expectedNotes = new ArrayList<Character>();
		expectedNotes.add('C');
		expectedNotes.add('D');
		expectedNotes.add('E');
		expectedNotes.add('F');

		// the actual result
		ArrayList<Character> result = new ArrayList<Character>();
		Collection<Character> symbols = score.getSymbols();
		for (Character symbol : symbols) {
			result.add(symbol);
		}

		assertEquals(expectedNotes, result);
	}

	@Test
	public void testGetBeatsPerMeasure() {
		Score score = new Score(4);
		assertEquals(4, score.getBeatsPerMeasure());
	}
}
