package br.ufrgs.inf.texttonoise.tests;

import static org.junit.Assert.assertEquals;

import java.io.FileReader;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import br.ufrgs.inf.texttonoise.Conductor;
import br.ufrgs.inf.texttonoise.InvalidCommandException;
import br.ufrgs.inf.texttonoise.NotationDictionary;
import br.ufrgs.inf.texttonoise.Score;

public class ConductorTest {

	NotationDictionary dictionary;

	@Before
	public void loadSampleDictionary() throws IOException {
		String testDictionaryFilepath = "src/test/resources/test-notes-execution-codes.json";
		FileReader testDictionaryFile = new FileReader(testDictionaryFilepath);
		dictionary = new NotationDictionary(testDictionaryFile);
		testDictionaryFile.close();
	}

	@Test
	public void testAlterTempo() {
		Conductor conductor = new Conductor(dictionary, new Score(4), 120);
		conductor.increaseTempo();
		int increasedTempo = conductor.getTempo();
		conductor.decreaseTempo();
		int decreasedTempo = conductor.getTempo();

		assertEquals(121, increasedTempo);
		assertEquals(120, decreasedTempo);
	}

	@Test
	public void testMinTempo() {
		Conductor conductor = new Conductor(dictionary, new Score(4),
				Conductor.minTempo);
		conductor.decreaseTempo();
		int decreasedTempo = conductor.getTempo();
		assertEquals(Conductor.minTempo, decreasedTempo);
	}

	@Test
	public void testMaxTempo() {
		Conductor conductor = new Conductor(dictionary, new Score(4),
				Conductor.maxTempo);
		conductor.increaseTempo();
		int increasedTempo = conductor.getTempo();
		assertEquals(Conductor.maxTempo, increasedTempo);
	}

	@Test
	public void testIncreaseVolume() {
		Conductor conductor = new Conductor(dictionary, new Score(4),
				Conductor.maxTempo);
		for (int i = 0; i <= 100; i++) {
			conductor.increaseVolume();
		}
		assertEquals(conductor.getCurrentVolume(), 127);
	}

	@Test
	public void testDecreaseVolume() {
		Conductor conductor = new Conductor(dictionary, new Score(4),
				Conductor.maxTempo);
		for (int i = 0; i <= 100; i++) {
			conductor.decreaseVolume();
		}
		assertEquals(conductor.getCurrentVolume(), 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNegativeTempoConstructor() {
		new Conductor(dictionary, new Score(4), -208);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testZeroTempoConstructor() {
		new Conductor(dictionary, new Score(4), 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testLessThanMinTempoConstructor() {
		new Conductor(dictionary, new Score(4), Conductor.minTempo - 1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testMoreThanMaxTempoConstructor() {
		new Conductor(dictionary, new Score(4), Conductor.maxTempo + 1);
	}

	@Test
	public void testCommandsExecutionsOnStartConcert() {
		Score score1 = new Score(4);
		score1.addSymbol('>');
		score1.addSymbol('+');
		score1.addSymbol('|');
		Conductor conductor = new Conductor(dictionary, score1,
				Conductor.minTempo);
		conductor.startConcert();

		assertEquals(Conductor.minTempo + 1, conductor.getTempo());
		assertEquals("started-concert played-last-measure finished-concert ",
				conductor.getLogMessage());
		assertEquals(Conductor.initialVolume + 1, conductor.getCurrentVolume());

		Score score2 = new Score(4);
		score2.addSymbol('<');
		score2.addSymbol('-');
		Conductor conductor2 = new Conductor(dictionary, score2,
				Conductor.maxTempo);
		conductor2.startConcert();

		assertEquals(Conductor.maxTempo - 1, conductor2.getTempo());
		assertEquals(Conductor.initialVolume - 1, conductor2.getCurrentVolume());
	}

	@Test(expected = InvalidCommandException.class)
	public void testInvalidCommandOnScore() {
		Score score1 = new Score(4);
		score1.addSymbol('@');
		Conductor conductor = new Conductor(dictionary, score1,
				Conductor.minTempo);
		conductor.startConcert();
	}

	@Test
	public void testPlayLastMeasure() {
		Conductor conductor = new Conductor(dictionary, new Score(4),
				Conductor.minTempo);
		conductor.playLastMeasure();
		assertEquals("played-last-measure ", conductor.getLogMessage());
	}

	@Test
	public void testStartConcert() {
		Score score = new Score(4);

		// create a simple score
		score.addSymbol('C');
		score.addSymbol('D');
		score.addSymbol('D');
		score.addSymbol('C');
		// Speed up, man
		for (int i = 0; i <= 100; i++)
			score.addSymbol('>');
		score.addSymbol('E');
		score.addSymbol('F');
		// Some pauses
		score.addSymbol('$');
		score.addSymbol('$');
		score.addSymbol('$');
		score.addSymbol('F');
		score.addSymbol('A');
		// wow, slow down, I'm getting confused
		for (int i = 0; i <= 50; i++)
			score.addSymbol('<');
		score.addSymbol('G');
		score.addSymbol('G');
		score.addSymbol('B');
		score.addSymbol('G');
		score.addSymbol('G');
		score.addSymbol('B');

		Conductor conductor = new Conductor(dictionary, score, 100);
		conductor.startConcert();

		assertEquals("started-concert finished-concert ",
				conductor.getLogMessage());
	}
}
