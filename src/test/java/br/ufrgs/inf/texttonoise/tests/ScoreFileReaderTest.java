package br.ufrgs.inf.texttonoise.tests;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;

import org.junit.Test;

import br.ufrgs.inf.texttonoise.Score;
import br.ufrgs.inf.texttonoise.ScoreFileReader;

public class ScoreFileReaderTest {

	@Test
	public void testGetScore() throws FileNotFoundException {
		String testFilePath = "src/test/resources/tiny-test-score.txt";
		ScoreFileReader scoreFile = new ScoreFileReader(testFilePath, 4);

		Score expectedScore = new Score(4);
		expectedScore.addSymbol('A');
		expectedScore.addSymbol('B');
		expectedScore.addSymbol('C');
		expectedScore.addSymbol('%');
		expectedScore.addSymbol('%');
		expectedScore.addSymbol('>');
		expectedScore.addSymbol('>');
		expectedScore.addSymbol('<');
		expectedScore.addSymbol('+');
		expectedScore.addSymbol('-');
		expectedScore.addSymbol('A');
		expectedScore.addSymbol('|');
		expectedScore.addSymbol('|');
		expectedScore.addSymbol('$');

		assertEquals(expectedScore, scoreFile.getScore());
	}
}
