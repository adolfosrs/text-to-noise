package br.ufrgs.inf.texttonoise.tests;

import static org.junit.Assert.assertEquals;

import java.io.FileReader;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import br.ufrgs.inf.texttonoise.Command;
import br.ufrgs.inf.texttonoise.Conductor;
import br.ufrgs.inf.texttonoise.DecreaseTempoCommand;
import br.ufrgs.inf.texttonoise.DecreaseVolumeCommand;
import br.ufrgs.inf.texttonoise.IncreaseTempoCommand;
import br.ufrgs.inf.texttonoise.IncreaseVolumeCommand;
import br.ufrgs.inf.texttonoise.NotationDictionary;
import br.ufrgs.inf.texttonoise.PlayLastMeasureCommand;
import br.ufrgs.inf.texttonoise.Score;

public class ConductorCommandsTest {
	NotationDictionary dictionary;

	@Before
	public void loadSampleDictionary() throws IOException {
		String testDictionaryFilepath = "src/test/resources/test-notes-execution-codes.json";
		FileReader testDictionaryFile = new FileReader(testDictionaryFilepath);
		dictionary = new NotationDictionary(testDictionaryFile);
		testDictionaryFile.close();
	}

	@Test
	public void testIncreaseTempo() {
		int initialTempo = Conductor.minTempo;
		Conductor aConductor = new Conductor(dictionary, new Score(4),
				initialTempo);

		Command command = new IncreaseTempoCommand(aConductor);

		command.execute();
		assertEquals(initialTempo + 1, aConductor.getTempo());
		command.execute();
		assertEquals(initialTempo + 2, aConductor.getTempo());
	}

	@Test
	public void testDecreaseTempo() {
		int initialTempo = Conductor.maxTempo;
		Conductor aConductor = new Conductor(dictionary, new Score(4),
				initialTempo);

		Command command = new DecreaseTempoCommand(aConductor);

		command.execute();
		assertEquals(initialTempo - 1, aConductor.getTempo());
		command.execute();
		assertEquals(initialTempo - 2, aConductor.getTempo());
	}

	@Test
	public void testPlayLastMeasure() {
		Conductor aConductor = new Conductor(dictionary, new Score(4),
				Conductor.minTempo);

		Command command = new PlayLastMeasureCommand(aConductor);
		command.execute();

		assertEquals("played-last-measure ", aConductor.getLogMessage());
		command.execute();
		assertEquals("played-last-measure played-last-measure ",
				aConductor.getLogMessage());
	}

	@Test
	public void testIncreaseVolume() {
		Conductor aConductor = new Conductor(dictionary, new Score(4),
				Conductor.minTempo);

		Command command = new IncreaseVolumeCommand(aConductor);
		command.execute();
		assertEquals(Conductor.initialVolume + 1, aConductor.getCurrentVolume());
		command.execute();
		assertEquals(Conductor.initialVolume + 2, aConductor.getCurrentVolume());
	}

	@Test
	public void testDecreaseVolume() {
		Conductor aConductor = new Conductor(dictionary, new Score(4),
				Conductor.minTempo);

		Command command = new DecreaseVolumeCommand(aConductor);
		command.execute();
		assertEquals(Conductor.initialVolume - 1, aConductor.getCurrentVolume());
		command.execute();
		assertEquals(Conductor.initialVolume - 2, aConductor.getCurrentVolume());
	}
}
